package com.icarus.laboratorio3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_resumen.*

class ResumenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resumen)
        val bundleRecepcion=intent.extras

        val nombre=bundleRecepcion!!.getString("key_nom")
        val edad=bundleRecepcion!!.getString("key_edad")
        val tipo=bundleRecepcion.getString("key_tipo").toString().toInt()
        val vacunas=bundleRecepcion!!.getString("key_vacunas")

        var nom_tipo:String=""
        val ima_tipo:String
        var lista_vacunas:String="No tiene vacunas aplicadas"
        tvNombre.text=nombre
        tvEdad.text=edad

        when(tipo){
            0-> {
                nom_tipo = "Perro"
                imvTipo.setImageResource(R.drawable.mascota_perro)
            }
            1-> {
                nom_tipo = "Gato"
                imvTipo.setImageResource(R.drawable.mascota_gato)
            }
            2-> {
                nom_tipo = "Conejo"
                imvTipo.setImageResource(R.drawable.mascota_conejo)
            }
        }
        tvTipo.text=nom_tipo
        if(!vacunas!!.equals("")){
            lista_vacunas=vacunas.substring(0,vacunas.length-1)
        }
        tvVacunas.text=lista_vacunas


    }
}