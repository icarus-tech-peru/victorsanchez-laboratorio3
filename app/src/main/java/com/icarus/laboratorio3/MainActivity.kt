package com.icarus.laboratorio3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnEnvio.setOnClickListener {
            var tipo:String=""
            var vac_rabia:String=""
            var vac_dist:String=""
            var vac_parvo:String=""
            var vac_leis:String=""
            var vacunas:String=""
            var nom=edtNombre.text.toString()
            var edad=edtEdad.text.toString()

            if(nom.isEmpty()){
                Toast.makeText(this,"Debe ingresar el nombre",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(edad.isEmpty()){
                Toast.makeText(this,"Debe ingresar la edad",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if(rbtPerro.isChecked) tipo="0"
            if(rbtGato.isChecked) tipo="1"
            if(rbtConejo.isChecked) tipo="2"

            if(chkRabia.isChecked) vac_rabia="Rabia," else vac_rabia=""
            if(chkDistemper.isChecked) vac_dist="Distemper," else vac_dist=""
            if(chkParvovirus.isChecked) vac_parvo="Parvovirus," else vac_parvo=""
            if(chkLehismania.isChecked) vac_leis="Leismaniasis," else vac_leis=""
            vacunas="$vac_rabia$vac_dist$vac_parvo$vac_leis"

            val bundle=Bundle().apply {
                putString("key_nom",nom)
                putString("key_edad",edad)
                putString("key_tipo",tipo)
                putString("key_vacunas",vacunas)
            }
            val intent=Intent(this,ResumenActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }
}